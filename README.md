# MarkdownWebConverter

A web server who take a mardown file in input and convert it into a web page (HTML, CSS)

## Convert without style

<u>Description</u><br/>
Send POST request to `/converter`. You will receive back a raw HTML page. **No CSS style is apply**

<u>URL</u>

```sh
(POST) https://$DOMAIN_NAME:$PORT/converter
```

<u>Request</u><br/>
`/converter` accept a JSON request, with a field text who have to be a string

```json
{
  "text": "type/string"
}
```

<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

---

To install linter (prettier, eslint) run
`yarn add --dev eslint prettier eslint-config-prettier eslint-plugin-prettier`
Add a conf file `.eslintrc.json` at the project's root. This file should contain

```json
{
  "extends": ["plugin:prettier/recommended"],

  "parserOptions": {
    "ecmaVersion": 2017
  }
}
```

To install dependencies for linter use

```sh
yarn install --production false
```

---

For tests start server node before testing. Because mocha can't start a node server (Module not found).
