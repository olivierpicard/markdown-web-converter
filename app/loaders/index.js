/**
 * A global loader, that include all sub loader
 * in a single command
 */
const loader_express = require('./express');

module.exports = {
  init: app => {
    loader_express(app);
  },
};
