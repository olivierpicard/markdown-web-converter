/**
 * This file will load all necessary modules
 * for express to work correctly. It will also setup
 * routes define in (__basedir/routes)
 */
const cors = require('cors');
const bodyParser = require('body-parser');
const routes = require(__basedir + '/routes/index');

async function init(app) {
  app.use(cors());

  app.use(bodyParser.json());
  app.use(
    bodyParser.urlencoded({
      extended: true,
    }),
  );

  app.use('/', routes);
}

module.exports = init;
