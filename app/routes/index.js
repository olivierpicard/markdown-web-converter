/**
 * Define all routes for this application
 * More complexe structure (path) will be
 * break into sub-module
 */
const router = require('express').Router();
const markdown_service = require(__basedir +
  '/services/markdown/index');

router.get('/ping', (req, res) => {
  res.send('pong');
});

router.post('/convert', (req, res) => {
  res.send(markdown_service.start(req.body.text));
});

module.exports = router;
