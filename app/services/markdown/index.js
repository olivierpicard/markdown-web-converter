/**
 * Markdown service. Here live
 * the service logic
 */

const md = require('markdown-it')({
  html: true,
  linkify: true,
  typographer: true,
});

module.exports = {
  start: text => {
    try {
      return md.render(text);
    } catch (error) {
      return null;
    }
  },
};
