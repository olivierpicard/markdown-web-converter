/**
 * The starting point of this project.
 * It will call loaders module to configure this project.
 */

global.__basedir = __dirname;

require('dotenv').config({ path: './config/.env' });
const express = require('express');
const loader = require('./loaders/index');
const app = express();

/// Load all modules and routes
loader.init(app);

function startServer() {
  app.listen(process.env.PORT, function() {
    console.log('Server listening on port ' + process.env.PORT);
    console.log(); // This allow a better print in terminal
  });
}

startServer();
