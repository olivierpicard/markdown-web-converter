global.__basedir = __dirname + '/..';

require('dotenv').config({ path: __dirname + '/config/.env' });
require('./express.test').test();
require('./markdown.service.test').test();
