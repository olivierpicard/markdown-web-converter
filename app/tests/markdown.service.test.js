const mocha = require('mocha');
const chai = require('chai');
const assert = require('assert');
const expect = chai.expect;
chai.use(require('chai-http'));

module.exports = {
  test: () => {
    describe('Markdown service', function() {
      it('Test bold text', function(done) {
        chai
          .request('http://localhost:3000')
          .post('/convert')
          .send({ text: '**Bold**' })
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res.text).to.contain('<strong>Bold</strong>');
            done();
          });
      });

      it('Test italic text', function(done) {
        chai
          .request('http://localhost:3000')
          .post('/convert')
          .send({ text: '*Italic*' })
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res.text).to.contain('<em>Italic</em>');
            done();
          });
      });

      it('Test underline text (HTML enable)', function(done) {
        chai
          .request('http://localhost:3000')
          .post('/convert')
          .send({ text: '<u>Underline</u>' })
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res.text).to.contain('<u>Underline</u>');
            done();
          });
      });

      it('Test Title h1', function(done) {
        chai
          .request('http://localhost:3000')
          .post('/convert')
          .send({ text: '# Text' })
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res.text).to.contain('<h1>Text</h1>');
            done();
          });
      });

      it('Test Title h6', function(done) {
        chai
          .request('http://localhost:3000')
          .post('/convert')
          .send({ text: '###### Text' })
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res.text).to.contain('<h6>Text</h6>');
            done();
          });
      });
    });
  },
};
