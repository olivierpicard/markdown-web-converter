const mocha = require('mocha');
const chai = require('chai');
const assert = require('assert');
const expect = chai.expect;
chai.use(require('chai-http'));

module.exports = {
  test: () => {
    describe('Express routes test', function() {
      describe('test /ping', function() {
        it('should return pong', function(done) {
          chai
            .request('http://localhost:3000')
            .get('/ping')
            .end((err, res) => {
              expect(err).to.be.null;
              expect(res.text).to.be.equal('pong');
              done();
            });
        });
      });
    });
  },
};
